import sys
import json

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def parser(body):
    body = body.decode('utf8').replace("'", '"')
    return json.loads(body)
