import pika
import os
import utils as utils
import redis_service as rs

RABBIT_HOST = os.getenv('RABBIT_HOST','localhost')

def handle_uploaded(bucket):
    rs.handle_uploaded(bucket)

def handle_extracted(bucket, total, targets):
    rs.handle_extracted(bucket, total, targets)

def handle_processed(bucket, object):
    rs.handle_processed(bucket, object)

def handle_compressed(bucket):
    rs.handle_compressed(bucket)

def handle_complete(bucket, object):
    rs.handle_complete(bucket, object)

def handle_error(bucket, error):
    rs.handle_error(bucket, error)

def handle_update_status(ch, method, properties, body):
    # extract message
    utils.eprint(" [x] Received %r" % body)
    parsed_body = utils.parser(body)
    status = parsed_body.get('status')
    bucket = parsed_body.get('bucket')
    if (status == 'uploaded'):
        handle_uploaded(bucket)
    elif (status == 'extracted'):
        total = parsed_body.get('total')
        targets = parsed_body.get('targets')
        handle_extracted(bucket, total, targets)
    elif (status == 'processed'):
        object = parsed_body.get('object')
        handle_processed(bucket, object)
    elif (status == 'compressed'):
        handle_compressed(bucket)
    elif (status == 'complete'):
        object = parsed_body.get('object')
        handle_complete(bucket, object)
    elif (status == 'error'):
        error = parsed_body.get('error')
        handle_error(bucket, error)
    else:
        utils.eprint('unknown status')
        return 'unknown status'

# Connect to RabbitMQ

connection = pika.BlockingConnection(pika.ConnectionParameters(host=RABBIT_HOST))
channel = connection.channel()

channel.queue_declare(queue='update_status', durable=True)

channel.basic_consume(handle_update_status,
                      queue='update_status',
                      no_ack=True)

print('Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
