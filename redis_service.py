import redis
import utils as utils
import os

REDIS_HOST = os.getenv('REDIS_HOST','localhost')

r = redis.StrictRedis(host=REDIS_HOST, port=6379, db=0)

def make_status_key(bucket):
    return bucket + ":status"

def make_total_key(bucket):
    return bucket + ":total"

def make_all_files_key(bucket):
    return bucket + ":all_files"

def make_p_counter_key(bucket):
    return bucket + ":p_counter"

def make_downloadable_key(bucket):
    return bucket + ":downloadable"

def make_error_key(bucket):
    return bucket + ":error"

def handle_uploaded(bucket):
    update_status(bucket, 'uploaded')

def handle_extracted(bucket, total, all_files):
    key = make_total_key(bucket)
    r.set(key, total)
    key = make_all_files_key
    r.set(key, all_files)
    update_status(bucket, 'extracted')

def handle_processed(bucket, object):
    key = make_p_counter_key(bucket)
    r.incr(key)
    update_status(bucket, 'processed')

def handle_compressed(bucket):
    update_status(bucket, 'compressed')

def handle_complete(bucket, downloadable):
    key = make_downloadable_key(bucket)
    r.set(key, downloadable)
    update_status(bucket, 'complete')

def handle_error(bucket, error):
    key = make_error_key(bucket)
    r.set(key, error)
    update_status(bucket, 'error')

def update_status(bucket, status):
    key = make_status_key(bucket)
    r.set(key, status)

def get_status(key):
    return r.get(key);

def increment_counter(key):
    return r.incr(key)

def redis():
    return r
